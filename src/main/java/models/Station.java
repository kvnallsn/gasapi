/**
 * Class representing a returned station from MyGasFeed.com
 */
package allison.kevin.cse691.gas.models;

public class Station {
  public final String country;
  public final int zip;
  public final String reg_price;
  public final String mid_price;
  public final String pre_price;
  public final String diesel_price;
  public final String address;
  public final int diesel;
  public final int id;
  public final double lat;
  public final double lng;
  public final String station;
  public final String region;
  public final String city;
  public final String reg_date;
  public final String mid_date;
  public final String pre_date;
  public final String diesel_date;
  public final String distance;

  public Station(String country, int zip, String reg_price, String mid_price, String pre_price,
      String diesel_price, String address, int diesel, int id, double lat, double lng,
      String station, String region, String city, String reg_date, String mid_date,
      String pre_date, String diesel_date, String distance) {

    this.country = country;
    this.zip = zip;
    this.reg_price = reg_price;
    this.mid_price = mid_price;
    this.pre_price = pre_price;
    this.diesel_price = diesel_price;
    this.address = address;
    this.diesel = diesel;
    this.id = id;
    this.lat = lat;
    this.lng = lng;
    this.station = station;
    this.region = region;
    this.city = city;
    this.reg_date = reg_date;
    this.mid_date = mid_date;
    this.pre_date = pre_date;
    this.diesel_date = diesel_date;
    this.distance = distance;
  }

  @Override
  public String toString() {
    return this.station + " (" + this.city + ")";
  }
}
