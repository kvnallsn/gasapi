/**
 * Class representing the returned geolocation from MyGasFeed.com
 */
package allison.kevin.cse691.gas.models;

public class GeoLocation {
  public final int cityId;
  public final String city;
  public final String regionShort;
  public final String region;
  public final String country;
  public final int countryId;
  public final int regionId;

  public GeoLocation(int city_id, String city_long, String region_short, String region_long,
      String country_long, int country_id, int region_id) {
  
    this.cityId = city_id;
    this.city = city_long;
    this.regionShort = region_short;
    this.region = region_long;
    this.country = country_long;
    this.countryId = country_id;
    this.regionId = region_id;
  }
}
