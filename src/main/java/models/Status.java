/**
 * Class representing the returned status from MyGasFeed.com
 */
package allison.kevin.cse691.gas.models;

public class Status {
  public final String error;
  public final int code;
  public final String description;
  public final String message;

  public Status(String error, int code, String description, String message) {
    this.error = error;
    this.code = code;
    this.description = description;
    this.message = message;
  }
}
