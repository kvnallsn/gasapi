/**
 * Class to hold the JSON data returned by MyGasFeed.com
 */
package allison.kevin.cse691.gas.holders;

import java.util.List;

import allison.kevin.cse691.gas.models.Status;
import allison.kevin.cse691.gas.models.GeoLocation;
import allison.kevin.cse691.gas.models.Station;

public class GasStationHolder {

  public final Status status;
  //public final GeoLocation geoLocation;
  public final List<Station> stations;

  //public GasStationHolder(Status status, GeoLocation geoLocation, List<Station> stations) {
  public GasStationHolder(Status status, List<Station> stations) {

    this.status = status;
    //this.geoLocation = geoLocation;
    this.stations = stations;
  }
}
