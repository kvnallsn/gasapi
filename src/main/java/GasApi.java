/**
 * Main file for implementing mygasfeed.com Java API
 */
package allison.kevin.cse691.gas;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import allison.kevin.cse691.gas.holders.GasStationHolder;
import allison.kevin.cse691.gas.models.Station;

interface MyGasFeed {
  @GET("stations/radius/{lat}/{long}/{distance}/{type}/{sort}/{apikey}.json")
    Call<GasStationHolder> nearbyStations(
        @Path("lat") double latitude,
        @Path("long") double longitude,
        @Path("distance") int distance,
        @Path("type") String fuelType,
        @Path("sort") String sortBy,
        @Path("apikey") String apiKey);
}

public final class GasApi {

  private static final GasApi myApi;
  private static String apiKey;

  static {
    myApi = new GasApi();
  }

  private MyGasFeed gasFeed;

  private GasApi() {
    // Set an increased timeout because this was written
    // in Guantanamo Bay, Cuba and the internet here blows
    // 56k circa 2016
    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
        .readTimeout(120, TimeUnit.SECONDS)
        .connectTimeout(120, TimeUnit.SECONDS)
        .build();

    // Create a simple REST adapter to point to MyGasFeed API
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("http://api.mygasfeed.com")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    this.gasFeed = retrofit.create(MyGasFeed.class);
  }

  public static void setApiKey(String myApiKey) {
    apiKey = myApiKey; 
  }

  public static List<Station> getNearbyStations(double latitude, double longitude,
      int distance, String fuelType, String sortBy) throws IOException {

    Call<GasStationHolder> call = myApi.gasFeed.nearbyStations(latitude, longitude, distance,
        fuelType, sortBy, apiKey);

    GasStationHolder holder = call.execute().body();

    return holder.stations;
  }

  public static void getNearbyStationsAsync(double latitude, double longitude,
      int distance, String fuelType, String sortBy, Callback<GasStationHolder> callback) {

    Call<GasStationHolder> call = myApi.gasFeed.nearbyStations(latitude, longitude, distance,
        fuelType, sortBy, apiKey);

    call.enqueue(callback);
  }
}
